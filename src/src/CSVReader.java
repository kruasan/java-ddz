package src;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class CSVReader {
    private final int HOUSES_ID = 0;
    private final int HOUSES_POPULATION = 5;
    private final int HOUSES_LATITUDE = 1;
    private final int HOUSES_LONGITUDE = 2;

    private final String fileHouses;

    public CSVReader(String fileHouses) { this.fileHouses = fileHouses; }

    public void importDb(DatabaseImporter database) {
        try (Reader readerHouses = new FileReader(fileHouses);
             CSVParser parserHouses = new CSVParser(readerHouses, CSVFormat.DEFAULT.withHeader())) {


            for (CSVRecord csvRecord : parserHouses) {
                Houses houses = new Houses();
                houses.setId(getIntValue(csvRecord, HOUSES_ID));
                houses.setPopulation(getIntValue(csvRecord, HOUSES_POPULATION));
                houses.setLatitude(getNumericValue(csvRecord, HOUSES_LATITUDE));
                houses.setLongitude(getNumericValue(csvRecord, HOUSES_LONGITUDE));

                database.insertHouses(houses);
            }

        } catch (IOException e) {
            System.out.println("Error reading CSV file: " + e.getMessage());
        }
    }

    private int getIntValue(CSVRecord csvRecord, int columnIndex) {
        try {
            return Integer.parseInt(csvRecord.get(columnIndex));
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private double getNumericValue(CSVRecord csvRecord, int columnIndex) {
        try {
            return Double.parseDouble(csvRecord.get(columnIndex));
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

}
