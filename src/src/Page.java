package src;

import java.util.ArrayList;
import java.util.Objects;

public class Page {
    private final ArrayList<ArrayList<String>> data;

    public Page(ArrayList<ArrayList<String>> data) {
        this.data = Objects.requireNonNullElseGet(data, ArrayList::new);
    }

    /**
     * @return (row, col) - элемент или null если такого элемента нет
     */
    public String get(int row, int col) {
        if (row < countRows() && col < countColumns())
            return data.get(row).get(col);
        return null;
    }

    public int countRows() {
        int size = 0;
        return data.size();
    }

    public int countColumns() {
        if (countRows() != 0)
            return data.get(0).size();
        return 0;
    }

    public ArrayList<ArrayList<String>> getData() {
        return data;
    }
}
