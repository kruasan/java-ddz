package src;


public class DomesticServices {
    public int id;
    public String latitude;
    public String name;
    public String objecttype;
    public String admindistrict;
    public String district;
    public String address;
    public String contactphone;
    public String workinghours;
    public String longitude;
    public int countofhouses;

    public DomesticServices(int id, String latitude, String name, String objecttype,
                            String admindistrict, String district, String address, String contactphone, String workinghours,
                            String longitude)
    {
        this.id = id;
        this.latitude = latitude;
        this.name = name;
        this.objecttype = objecttype;
        this.admindistrict = admindistrict;
        this.district = district;
        this.address = address;
        this.contactphone = contactphone;
        this.workinghours = workinghours;
        this.longitude = longitude;
        this.countofhouses = 0;

    }
    public DomesticServices(int id, String latitude, String name, String objecttype,
                            String admindistrict, String district, String address, String contactphone,
                            String workinghours, String longitude, int countofhouses)
    {
        this.id = id;
        this.latitude = latitude;
        this.name = name;
        this.objecttype = objecttype;
        this.admindistrict = admindistrict;
        this.district = district;
        this.address = address;
        this.contactphone = contactphone;
        this.workinghours = workinghours;
        this.longitude = longitude;
        this.countofhouses = countofhouses;

    }

    public DomesticServices() {

    }

    public String getLatitude() {
        return this.latitude;
    }

    public String getObjectType() {
        return this.objecttype;
    }

    public String getAdminDistrict() {
        return this.admindistrict;
    }

    public String getDistrict() {
        return this.district;
    }

    public String getAddress() {
        return this.address;
    }

    public String getContactPhone() {
        return this.contactphone;
    }

    public String getWorkingHours() {
        return this.workinghours;
    }


    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String  getLongitude() { return this.longitude; }

    public int getCountOfHouses() { return this.countofhouses; }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setObjectType(String objectType) {
        this.objecttype = objecttype;
    }

    public void setAdminDistrict(String admindistrict) {
        this.admindistrict = admindistrict;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setContactPhone(String contactphone) {
        this.contactphone = contactphone;
    }

    public void setWorkingHours(String workingHours) {
        this.workinghours = workingHours;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setCountOfHouses(int countofhouses) { this.countofhouses  = countofhouses; }

    public String toString() {
        return "id: " + this.id + "\nname: " + this.name + "\nlatitude: " + this.latitude + "\nobjecttype" + this.objecttype +
                "\nadmindistrict" + this.admindistrict + "\ndistrict" + this.district + "\naddress" + this.address +
                "\ncontactphone" + this.contactphone + "\nworkinghours" + this.workinghours + "\nlongitude: " + this.longitude +
                "\ncountofhouses" + this.countofhouses + "\n";
    }
}