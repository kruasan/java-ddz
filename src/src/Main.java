package src;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Filter;

public class Main {
    public static void parseData(String path, int tableNum) throws SQLException {
        DatabaseImporter storage = new DatabaseImporter("jdbc:sqlite:resources/database/database.db");
        storage.createTables();
        String xlsxfilepath = "resources/database/data-4272-2023-09-13.xlsx";
        XlsxReader xlsxReader = new XlsxReader(xlsxfilepath);
        String jsonfilepath = "resources/database/metro.json";
        JsonReader jsonReader = new JsonReader(jsonfilepath);
        String csvfilepath = "resources/database/Houses.csv";
        CSVReader csvReader = new CSVReader(csvfilepath);
        Scanner scanner = new Scanner(System.in);
        int swth;
        String filter = null;

        while (true) {
            System.out.println("Выберите действие:\n" +
                    "1. Импорт данных\n" +
                    "2. Подсчет статистики по количеству пунктов бытовых услуг на каждую станцию метро\n" +
                    "3. Подсчет статистики по количеству жителей на каждый из пунктов бытовых услуг\n" +
                    "4. Постраничный вывод пунктов бытовых услуг\n" +
                    "5. Выход");
            swth = scanner.nextInt();

            switch (swth) {
                case 1: {
                    System.out.println("Import Domestic Services\n");
                    xlsxReader.readDomesticServices(storage);
                    System.out.println("Import Metro Stations\n");
                    jsonReader.readMetroStations(storage);
                    System.out.println("Import Houses\n");
                    csvReader.importDb(storage);
                    break;
                }
                case 2: {
                    storage.metroStationsStatistic();

                    ArrayList<MetroStation> metroStations = new ArrayList<>();
                    metroStations = storage.getMetroStations();

                    for (MetroStation metroStation : metroStations) {
                        System.out.println(metroStation.getName() + ": " + metroStation.countOfServices);
                    }
                    break;
                }
                case 3: {
                    storage.PopulationStatistic();

                    ArrayList<DomesticServices> services = new ArrayList<>();
                    services = storage.getService();

                    for (DomesticServices service : services) {
                        System.out.println(service.getName() + ": " + service.countofhouses);
                    }
                    break;
                }
                case 4: {
                    int skip = 0;
                    int choice = 0;

                    while (choice != 11) {
                        System.out.println("Выберите округ:\n" +
                                "1. Северо-Восточный административный округ\n" +
                                "2. Центральный административный округ\n" +
                                "3. Южный административный округ\n" +
                                "4. Северный административный округ\n" +
                                "5. Восточный административный округ\n" +
                                "6. Западный административный округ\n" +
                                "7. Северо-Западный административный округ\n" +
                                "8. Юго-Восточный административный округ\n" +
                                "9. Юго-Западный административный округ\n" +
                                "10. Выход\n");
                        choice = scanner.nextInt();
                        System.out.print("Введите страницу\n");
                        skip = scanner.nextInt();
                        switch (choice) {
                            case 1: {
                                filter = "Северо-Восточный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 2: {
                                filter = "Центральный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 3: {
                                filter = "Южный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 4: {
                                filter = "Северный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 5: {
                                filter = "Восточный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 6: {
                                filter = "Западный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 7: {
                                filter = "Северо-Западный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 8: {
                                filter = "Юго-Восточный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }
                            case 9: {
                                filter = "Юго-Западный административный округ";
                                ArrayList<DomesticServices> service = storage.getServiceFilter(filter, skip);
                                System.out.println(service);
                                break;
                            }

                        }
                    }
                    break;
                }
                case 5: {
                    storage.closeDB();
                    return;
                }
                default: {
                    System.out.print("Wrong number!\n");
                }
            }
        }
    }

    public static void main(String[] args) throws SQLException {
        parseData("resources/database/", 0);
    }
}
