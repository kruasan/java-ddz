package src;

public class MetroStation {
    public double id;
    public String name;
    public double latitude;
    public double longitude;
    public int countOfServices;

    public MetroStation(double id, String name, double latitude, double longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.countOfServices = 0;
    }

    public MetroStation(double id, String name, double latitude, double longitude, int countOfServices) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
        this.countOfServices = countOfServices;
    }

    public double getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public double getLatitude() {
        return this.latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }

    public int getCountOfServices() {
        return this.countOfServices;
    }

    public String toString() {
        return "id: " + this.id + "\nname: " + this.name + "\nlatitude: " + this.latitude + "\nlongitude: " + this.longitude + "\ncountOfServices: " + this.countOfServices + "\n";
    }
}
