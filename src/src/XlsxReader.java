package src;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class XlsxReader {
    private String pathToXlsxFile;

    public XlsxReader(String pathToXlsxFile) {
        this.pathToXlsxFile = pathToXlsxFile;
    }

    public void readDomesticServices(DatabaseImporter database) {
        try (FileInputStream fileInputStream = new FileInputStream(new File(pathToXlsxFile))) {
            Workbook workbook = WorkbookFactory.create(fileInputStream);
            Sheet sheet = workbook.getSheetAt(0); // Предполагаем, что данные находятся в первом листе
            int rowsCount = sheet.getLastRowNum();

            for (int i = 2; i <= rowsCount; i++) { // Начинаем с 2, чтобы пропустить заголовок
                Row row = sheet.getRow(i);
                if (row != null) {
                    try {
                        // Считываем значения из ячеек строки
                        String idStr = row.getCell(1).getStringCellValue();
                        int id = Integer.parseInt(idStr);
                        String latitude = row.getCell(0).getStringCellValue();
                        String name = row.getCell(2).getStringCellValue();
                        String object_type = row.getCell(7).getStringCellValue();
                        String admin_district = row.getCell(8).getStringCellValue();
                        String district = row.getCell(9).getStringCellValue();
                        String address = row.getCell(10).getStringCellValue();
                        String contact_phone = row.getCell(11).getStringCellValue();
                        String working_hours = row.getCell(12).getStringCellValue();
                        String longitude = row.getCell(14).getStringCellValue();

                        // Создаем объект отеля и добавляем его в базу данных
                        DomesticServices service = new DomesticServices(id, latitude, name, object_type, admin_district, district, address, contact_phone, working_hours, longitude);
                        database.insertDomesticServices(service);

                    } catch (Exception e) {
                        System.out.println("Ошибка при чтении данных из строки " + (i + 1) + ": " + e.getMessage());
                    }
                }
            }
            workbook.close();
        } catch (IOException | EncryptedDocumentException ex) {
            ex.printStackTrace();
        }
    }



    private int parseInt(String number)
    {
        number = number.trim().replaceAll("\\D", "");
        number = number.isEmpty() ? "0" : number;

        return Integer.parseInt(number);
    }

    private double parseDouble(String number)
    {
        number = number.trim().replaceAll(",", ".").replaceAll("[^0-9.]", "");//.replaceAll("\\s", "");
        number = number.isEmpty() ? "0" : number;

        return Double.parseDouble(number);
    }
}
