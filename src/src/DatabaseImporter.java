package src;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;


public class DatabaseImporter {
    public Connection connection;
    public Statement statement;
    private PreparedStatement insertDomesticServices;
    private PreparedStatement insertMetroStation;
    private PreparedStatement insertHouses;

    static {
        int countofmetstatic = 0;
        int countofhousestatic = 0;
    }


    public DatabaseImporter(String url) {
        try {
            this.connection = DriverManager.getConnection(url);
            this.connection.prepareStatement("PRAGMA encoding = 'UTF-8';");
            this.statement = this.connection.createStatement();
        } catch (SQLException exception) {
            System.out.println("Error! " + exception);
        }

    }

    public void createTables() {
        String createServiceStatement = "CREATE TABLE IF NOT EXISTS services" +
                "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "latitude TEXT, " +
                "name TEXT, " +
                "objecttype TEXT, " +
                "admindistrict TEXT, " +
                "district TEXT, " +
                "address TEXT, " +
                "contactphone TEXT, " +
                "workinghours TEXT, " +
                "longitude TEXT, " +
                "countofhouses INTEGER" +
                ");";

        String insertServiceStatement = "INSERT INTO services " +
                "(id, latitude, name, objecttype, admindistrict, district, address, contactphone, workinghours, longitude, countofhouses) " +
                "VALUES " +
                "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

        String createMetroStationStatement = "CREATE TABLE IF NOT EXISTS metrostations" +
                "(" +
                "id DOUBLE PRIMARY KEY, " +
                "name TEXT, " +
                "latitude DOUBLE, " +
                "longitude DOUBLE, " +
                "countOfServices INTEGER " +
                ");";

        String insertMetroStationStatement = "INSERT INTO metrostations " +
                "(id, name, latitude, longitude, countOfServices) " +
                "VALUES " +
                "(?, ?, ?, ?, ?);";

        String createHousesStatement = "CREATE TABLE IF NOT EXISTS Houses" +
                "(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "Population INTEGER," +
                "Latitude DOUBLE, " +
                "Longitude DOUBLE" +
                ");";

        String insertHousesStatement = "INSERT INTO Houses " +
                "(id, Population, latitude, longitude) " +
                "VALUES " +
                "(?, ?, ?, ?);";

        try {
            PreparedStatement createService = connection.prepareStatement(createServiceStatement);
            createService.execute();
            insertDomesticServices = connection.prepareStatement(insertServiceStatement);

            PreparedStatement createMetroStation = connection.prepareStatement(createMetroStationStatement);
            createMetroStation.execute();
            insertMetroStation = connection.prepareStatement(insertMetroStationStatement);

            PreparedStatement createHouses = connection.prepareStatement(createHousesStatement);
            createHouses.execute();
            insertHouses = connection.prepareStatement(insertHousesStatement);

        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    public void insertDomesticServices(DomesticServices service) {
        try {
            this.connection.setAutoCommit(false);
            this.insertDomesticServices.setInt(1, service.getId());
            this.insertDomesticServices.setString(2, service.getLatitude());
            this.insertDomesticServices.setString(3, service.getName());
            this.insertDomesticServices.setString(4, service.getObjectType());
            this.insertDomesticServices.setString(5, service.getAdminDistrict());
            this.insertDomesticServices.setString(6, service.getDistrict());
            this.insertDomesticServices.setString(7, service.getAddress());
            this.insertDomesticServices.setString(8, service.getContactPhone());
            this.insertDomesticServices.setString(9, service.getWorkingHours());
            this.insertDomesticServices.setString(10, service.getLongitude());
            this.insertDomesticServices.setInt(11, service.getCountOfHouses());
            this.insertDomesticServices.execute();
            this.connection.commit();
            this.connection.setAutoCommit(true);
        } catch (SQLException exception) {
            System.out.println("Write error! " + String.valueOf(exception));
        }
    }

    public void insertMetroStation(MetroStation metroStation) {
        try {
            this.connection.setAutoCommit(false);
            this.insertMetroStation.setDouble(1, metroStation.getId());
            this.insertMetroStation.setString(2, metroStation.getName());
            this.insertMetroStation.setDouble(3, metroStation.getLatitude());
            this.insertMetroStation.setDouble(4, metroStation.getLongitude());
            this.insertMetroStation.setInt(5, metroStation.getCountOfServices());
            this.insertMetroStation.execute();
            this.connection.commit();
            this.connection.setAutoCommit(true);
        } catch (SQLException exception) {
            System.out.println("Write error! " + String.valueOf(exception));
        }
    }

    public void insertHouses(Houses houses) {
        try {
            this.connection.setAutoCommit(false);
            insertHouses.setInt(1, houses.getId());
            insertHouses.setInt(2, houses.getPopulation());
            insertHouses.setDouble(3, houses.getLatitude());
            insertHouses.setDouble(4, houses.getLongitude());
            insertHouses.execute();
            this.connection.commit();
            this.connection.setAutoCommit(true);
        } catch (SQLException e) {
            System.err.println("Error writing metro station: " + e.getMessage());
            throw new RuntimeException("Error writing metro station", e);
        }
    }

    private void updateMetroStation(String name, int count) {
        try {
            statement.executeUpdate("UPDATE metrostations SET countOfServices=" +
                    count + " WHERE name=\"" + name + "\";");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void updateDomesticServices(String name, int count) {
        try {
            statement.executeUpdate("UPDATE services SET countofhouses=" +
                    count + " WHERE name=\"" + name + "\";");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<DomesticServices> getService() {
        ArrayList<DomesticServices> services = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM services ORDER BY name;");

            while (resultSet.next()) {
                DomesticServices service = new DomesticServices(resultSet.getInt("id"),
                        resultSet.getString("latitude"),
                        resultSet.getString("name"),
                        resultSet.getString("objecttype"),
                        resultSet.getString("admindistrict"),
                        resultSet.getString("district"),
                        resultSet.getString("address"),
                        resultSet.getString("contactphone"),
                        resultSet.getString("workinghours"),
                        resultSet.getString("longitude"),
                        resultSet.getInt("countofhouses"));
                services.add(service);
            }

        } catch (SQLException e) {
            System.err.println("Error reading services: " + e.getMessage());
        }

        return services;
    }

    public ArrayList<DomesticServices> filter(String str) {
        ArrayList<DomesticServices> services = new ArrayList<>();
        ResultSet resultSet;

        try {
            resultSet = statement.executeQuery("SELECT * FROM services WHERE admindistrict LIKE \"" + str + "%\";");

            while (resultSet.next()) {
                DomesticServices services1 = new DomesticServices();

                services1.setLatitude(resultSet.getString("latitude"));
                services1.setName(resultSet.getString("name"));
                services1.setObjectType(resultSet.getString("objecttype"));
                services1.setAdminDistrict(resultSet.getString("admindistrict"));
                services1.setDistrict(resultSet.getString("district"));
                services1.setAddress(resultSet.getString("address"));
                services1.setContactPhone(resultSet.getString("contactphone"));
                services1.setWorkingHours(resultSet.getString("workinghours"));
                services1.setLongitude(resultSet.getString("longitude"));
                services1.setCountOfHouses(resultSet.getInt("countofhouses"));

                services.add(services1);
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }

        return services;
    }

    public ArrayList<DomesticServices> getServiceFilter(String admindistrict, int skip) {
        ArrayList<DomesticServices> services = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM services " +
                    "WHERE admindistrict LIKE '" + admindistrict + "%'" + " LIMIT 15 OFFSET " + 15 * skip + ";");


            while (resultSet.next()) {
                DomesticServices service = new DomesticServices(resultSet.getInt("id"),
                        resultSet.getString("latitude"),
                        resultSet.getString("name"),
                        resultSet.getString("objecttype"),
                        resultSet.getString("admindistrict"),
                        resultSet.getString("district"),
                        resultSet.getString("address"),
                        resultSet.getString("contactphone"),
                        resultSet.getString("workinghours"),
                        resultSet.getString("longitude"),
//                        resultSet.getInt("countofmetrostations"),
                        resultSet.getInt("countofhouses"));
                services.add(service);
            }

        } catch (SQLException e) {
            System.err.println("Error reading services: " + e.getMessage());
        }

        return services;
    }

    public ArrayList<MetroStation> getMetroStations() {
        ArrayList<MetroStation> metroStations = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM metrostations;");

            while (resultSet.next()) {
                MetroStation metroStation = new MetroStation(resultSet.getDouble("id"),
                        resultSet.getString("name"),
                        resultSet.getDouble("latitude"),
                        resultSet.getDouble("longitude"),
                        resultSet.getInt("countOfServices"));

                metroStations.add(metroStation);
            }

        } catch (SQLException e) {
            System.err.println("Error reading metro stations: " + e.getMessage());
        }

        return metroStations;
    }

    public ArrayList<Houses> getHouses() {
        ArrayList<Houses> houses = new ArrayList<>();

        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM Houses;");

            while (resultSet.next()) {
                Houses house = new Houses();
                house.setId(resultSet.getInt("id"));
                house.setPopulation(resultSet.getInt("Population"));
                house.setLongitude(resultSet.getDouble("Longitude"));
                house.setLatitude(resultSet.getDouble("Latitude"));

                houses.add(house);
            }
        } catch (SQLException e) {
            System.err.println("Error reading metro stations: " + e.getMessage());
        }

        return houses;
    }
    private static double toRadian(double number) {
        return number * Math.PI / 180;
    }

    private static double getDistance(double lon1, double lat1, double lon2, double lat2) {
        double radLat1 = toRadian(lat1);
        double radLat2 = toRadian(lat2);
        double latDifference = radLat1 - radLat2;
        double lonDifference = toRadian(lon1) - toRadian(lon2);
        double dist = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(latDifference / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(lonDifference / 2), 2)));

        return dist * 6378137;
    }

    public void PopulationStatistic() throws SQLException {
        ArrayList<Houses> houses = getHouses();
        ArrayList<DomesticServices> services = getService();
        connection.setAutoCommit(false);
        for (DomesticServices service : services) {
            service.countofhouses = 0;
            double maxDistance = 100;
            for (Houses house : houses) {
                double distance = getDistance(
                        house.getLongitude(),
                        house.getLatitude(),
                        Double.parseDouble(service.getLongitude()),
                        Double.parseDouble(service.getLatitude())
                );

                if (distance <= maxDistance) {
                    service.countofhouses += house.getPopulation();
                    updateDomesticServices(service.name, service.countofhouses);
                }
            }
        }
        connection.commit();
        connection.setAutoCommit(true);
    }

    public void metroStationsStatistic() throws SQLException {
        ArrayList<MetroStation> metroStations = getMetroStations();
        ArrayList<DomesticServices> services = getService();
        this.connection.setAutoCommit(false);
//        int i = 0;
        for (MetroStation metroStation : metroStations) {
            metroStation.countOfServices = 0;
            double maxDistance = 500;

            for (DomesticServices service : services) {
                double distance = getDistance(
                        metroStation.getLongitude(),
                        metroStation.getLatitude(),
                        Double.parseDouble(service.getLongitude()),
                        Double.parseDouble(service.getLatitude())
                );

                if (distance <= maxDistance) {
                    metroStation.countOfServices += 1;
                    updateMetroStation(metroStation.name, metroStation.countOfServices);
                }
            }

//            i++;
//            System.out.println(i);
        }
        this.connection.commit();
        this.connection.setAutoCommit(true);
    }

    public boolean isEmpty() {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT 1 FROM services LIMIT 1");

            if (resultSet.next()) {
                return false;
            }

            return true;
        } catch (SQLException e) {
            System.err.println("Error checking if the database is empty: " + e.getMessage());
            throw new RuntimeException("Error checking if the database is empty", e);
        }
    }

    public void closeDB() {
        try {
            this.connection.close();
            this.statement.close();
        } catch (SQLException exception) {
            System.out.println("Error! " + exception);
        }
    }
}
