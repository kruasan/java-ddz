package src;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;


public class JsonReader {
    private String pathToJsonFile;

    public JsonReader(String pathToJsonFile) {
        this.pathToJsonFile = pathToJsonFile;
    }

    public void readMetroStations(DatabaseImporter database) {
        String name;
        double id, longitude, latitude;
        JSONParser parser = new JSONParser();

        try (Reader reader = Files.newBufferedReader(Paths.get(pathToJsonFile))) {
            Object obj = parser.parse(reader);
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray lines = (JSONArray) jsonObject.get("lines");

            for (Object lineObj : lines) {
                JSONObject line = (JSONObject) lineObj;
                JSONArray stations = (JSONArray) line.get("stations");

                for (Object stationObj : stations) {
                    JSONObject station = (JSONObject) stationObj;

                    id = Double.parseDouble((String) station.get("id"));
                    name = (String) station.get("name");
                    latitude = (Double) station.get("lat");
                    longitude = (Double) station.get("lng");

                    MetroStation metroStation = new MetroStation(id, name, latitude, longitude);

                    database.insertMetroStation(metroStation);
                }
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
    }

    private double parseDouble(String number)
    {
        number = number.trim().replaceAll(",", ".").replaceAll("[^0-9.]", "");//.replaceAll("\\s", "");
        number = number.isEmpty() ? "0" : number;

        return Double.parseDouble(number);
    }
}
