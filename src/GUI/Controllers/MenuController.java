package GUI.Controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import src.*;

import static java.lang.Math.ceil;
import static java.lang.Math.floor;

public class MenuController implements Initializable {
    private static DatabaseImporter database = null;
    private final int itemsPerPage = 20;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        serviceLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));
        serviceName.setCellValueFactory(new PropertyValueFactory<>("Name"));
        serviceObjectType.setCellValueFactory(new PropertyValueFactory<>("ObjectType"));
        serviceAdminDistrict.setCellValueFactory(new PropertyValueFactory<>("AdminDistrict"));
        serviceDistrict.setCellValueFactory(new PropertyValueFactory<>("District"));
        serviceAddress.setCellValueFactory(new PropertyValueFactory<>("Address"));
        serviceContactPhone.setCellValueFactory(new PropertyValueFactory<>("ContactPhone"));
        serviceWorkingHours.setCellValueFactory(new PropertyValueFactory<>("WorkingHours"));
        serviceCountOfHouses.setCellValueFactory(new PropertyValueFactory<>("CountOfHouses"));
        serviceLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));


        metStatName.setCellValueFactory(new PropertyValueFactory<>("Name"));
        metStatLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
        metStatLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));


        housesLon.setCellValueFactory(new PropertyValueFactory<>("Longitude"));
        housesLat.setCellValueFactory(new PropertyValueFactory<>("Latitude"));
        housesPop.setCellValueFactory(new PropertyValueFactory<>("Population"));

        Name.setCellValueFactory(new PropertyValueFactory<>("Name"));
        CountOfHouses.setCellValueFactory(new PropertyValueFactory<>("CountOfHouses"));

        name.setCellValueFactory(new PropertyValueFactory<>("name"));
        countOfServices.setCellValueFactory(new PropertyValueFactory<>("countOfServices"));

        servicePagination.setVisible(false);
        metStatPagination.setVisible(false);
        housesPagination.setVisible(false);
        popPagination.setVisible(false);
        metroStatisticPagination.setVisible(false);
    }

    @FXML
    private TabPane tabPane;
    @FXML
    private Tab serviceTab;
    @FXML
    private TableView<DomesticServices> serviceTable;
    @FXML
    private TableColumn<DomesticServices, Double> serviceLat;
    @FXML
    private TableColumn<DomesticServices, String> serviceName;
    @FXML
    private TableColumn<DomesticServices, String> serviceObjectType;
    @FXML
    private TableColumn<DomesticServices, String> serviceAdminDistrict;
    @FXML
    private TableColumn<DomesticServices, String> serviceDistrict;
    @FXML
    private TableColumn<DomesticServices, String> serviceAddress;
    @FXML
    private TableColumn<DomesticServices, String> serviceContactPhone;
    @FXML
    private TableColumn<DomesticServices, String> serviceWorkingHours;
    @FXML
    private TableColumn<DomesticServices, Double> serviceLon;
    @FXML
    private TableColumn<DomesticServices, Integer> serviceCountOfHouses;
    @FXML
    private Pagination servicePagination;
    @FXML
    private Tab metStatTab;
    @FXML
    private TableView<MetroStation> metStatTable;
    @FXML
    private TableColumn<MetroStation, String> metStatName;
    @FXML
    private TableColumn<MetroStation, Double> metStatLon;
    @FXML
    private TableColumn<MetroStation, Double> metStatLat;
    @FXML
    private Pagination metStatPagination;
    @FXML
    private Tab housesTab;
    @FXML
    private TableView<Houses> housesTable;
    @FXML
    private TableColumn<Houses, Double> housesLon;
    @FXML
    private TableColumn<Houses, Double> housesLat;
    @FXML
    private TableColumn<Houses, Integer> housesPop;
    @FXML
    private Pagination housesPagination;
    @FXML
    private AnchorPane importPane;
    @FXML
    private Label labelImport;
    @FXML
    private Button buttonImport;
    @FXML
    private TextField importField;
    @FXML
    private Label databaseName;
    @FXML
    private AnchorPane filterPane;
    @FXML
    private TextField filterField;
    @FXML
    private Label labelFilter;
    @FXML
    private Button buttonFilter;

    @FXML
    private Tab popTab;

    @FXML
    private Tab metroStatisticTab;

    @FXML
    private TableView<DomesticServices> popTable;

    @FXML
    private TableColumn<DomesticServices, String> Name;

    @FXML
    private TableColumn<DomesticServices, Integer> CountOfHouses;

    @FXML
    private Pagination popPagination;

    @FXML
    private TableView<MetroStation> metroStatisticTable;

    @FXML
    private TableColumn<MetroStation, String> name;

    @FXML
    private TableColumn<MetroStation, Integer> countOfServices;

    @FXML
    private Pagination metroStatisticPagination;

    @FXML
    void importDatabase() {

        String path = importField.getText();
        File file = new File(path);

        if (path.isEmpty()) {

            try {
                String url = "jdbc:sqlite:resources/database/database.db";
                database = new DatabaseImporter(url);

                try {
                    database.isEmpty();
                } catch (Exception e) {
                    database.createTables();

                    String xlsxfilepath = "resources/database/data-4272-2023-09-13.xlsx";
                    String jsonfilepath = "resources/database/metro.json";
                    String csvfilepath = "resources/database/Houses.csv";

                    JsonReader readerJSON = new JsonReader(jsonfilepath);
                    CSVReader readerCSV = new CSVReader(csvfilepath);
                    XlsxReader readerXLSX = new XlsxReader(xlsxfilepath);

                    readerJSON.readMetroStations(database);
                    readerCSV.importDb(database);
                    readerXLSX.readDomesticServices(database);
                }

                reloadDatabase();

                servicePagination.setVisible(true);
                metStatPagination.setVisible(true);
                housesPagination.setVisible(true);
                popPagination.setVisible(true);
                metroStatisticPagination.setVisible(true);

                databaseName.setText(path);
                importPane.setVisible(true);
                labelImport.setStyle("-fx-text-fill: black;");
                labelImport.setAlignment(Pos.CENTER);
                importField.setVisible(false);
                labelImport.setText("Data were updated by default paths!");

                buttonImport.setVisible(false);
            } catch (Exception e) {
                importPane.setVisible(true);
                labelImport.setStyle("-fx-text-fill: red;");
                labelImport.setAlignment(Pos.CENTER);
                labelImport.setText("Error! Necessary files by default did not exist!");
                importField.setVisible(false);
                buttonImport.setVisible(false);
            }
            return;
        }

        if (!file.exists() || file.isDirectory()) {
            importPane.setVisible(true);
            labelImport.setStyle("-fx-text-fill: red;");
            labelImport.setAlignment(Pos.CENTER);
            labelImport.setText("Error! Your path is wrong!");
            importField.setVisible(false);
            buttonImport.setVisible(false);
            return;
        }

        try {
            String URL = "jdbc:sqlite:" + path;
            database = new DatabaseImporter(URL);
            reloadDatabase();
            servicePagination.setVisible(true);
            metStatPagination.setVisible(true);
            housesPagination.setVisible(true);
            popPagination.setVisible(true);
            metroStatisticPagination.setVisible(true);

            databaseName.setText(path);
            importPane.setVisible(true);
            labelImport.setStyle("-fx-text-fill: black;");
            labelImport.setAlignment(Pos.CENTER);
            importField.setVisible(false);
            labelImport.setText("Data were updated!");
            buttonImport.setVisible(false);
        } catch (Exception e) {
            System.err.println("Error! Database were broke!");
        }
    }

    @FXML
    void exit() {
        Stage primaryStage = (Stage) tabPane.getScene().getWindow();
        primaryStage.close();
    }

    @FXML
    void back() {
        importPane.setVisible(false);
        filterPane.setVisible(false);
    }

    @FXML
    void importData() {
        importPane.setVisible(true);
        labelImport.setStyle("-fx-text-fill: black;");
        labelImport.setAlignment(Pos.CENTER);
        labelImport.setText("Import database (clear field for import by default):");
        importField.setText("");
        importField.setVisible(true);
        buttonImport.setVisible(true);
    }

    @FXML
    void filterDistrict() {
        filterPane.setVisible(true);

        if (database == null || database.isEmpty()) {
            labelFilter.setStyle("-fx-text-fill: red;");
            labelFilter.setAlignment(Pos.CENTER);
            labelFilter.setText("Error! You did not import data!");
            filterField.setVisible(false);
            buttonFilter.setVisible(false);
        } else {
            labelFilter.setStyle("-fx-text-fill: black;");
            labelFilter.setAlignment(Pos.CENTER);
            labelFilter.setText("Write name of metro stations:");
            filterField.setVisible(true);
            buttonFilter.setVisible(true);
        }
    }

//    private static ProgressController progressController = new ProgressController();

    @FXML
    void calculateStatistics(ActionEvent event) throws SQLException {
        try {
            // Загрузка FXML-файла для нового окна
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Page/Progress.fxml"));
            Parent root = loader.load();

            // Получение контроллера из загрузчика
            ProgressController progressController = loader.getController();

            // Запуск задачи через контроллер
            if (progressController != null) {
                progressController.startTask(this);
            } else {
                System.err.println("Error: ProgressController is not initialized!");
            }

            // Создание новой сцены
            Scene scene = new Scene(root);

            // Создание нового окна и установка сцены
            Stage stage = new Stage();
            stage.setScene(scene);

            stage.setWidth(400);
            stage.setHeight(300);

            // Отображение окна
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    private void openStatisticsWindow() {
//        try {
//            // Загрузка FXML-файла для нового окна
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("/Page/Progress.fxml"));
//            Parent root = loader.load();
//
//            // Создание новой сцены
//            Scene scene = new Scene(root);
//
//            // Создание нового окна и установка сцены
//            Stage stage = new Stage();
//            stage.setScene(scene);
//
//            // Отображение окна
//            stage.show();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    // Метод для обновления таблицы статистики населения
    void updatePopulationTable() {
        ArrayList<DomesticServices> services = database.getService();
        ObservableList<DomesticServices> ServiceData = FXCollections.observableArrayList(services);
        popTable.setItems(ServiceData);
    }

    // Метод для обновления таблицы статистики станций метро
    void updateMetroStationTable() {
        ArrayList<MetroStation> metroStations = database.getMetroStations();
        ObservableList<MetroStation> metroStationData = FXCollections.observableArrayList(metroStations);
        metStatTable.setItems(metroStationData);
    }

    @FXML
    public void filter() {
        ArrayList<DomesticServices> list;
        SingleSelectionModel<Tab> selectionModel = tabPane.getSelectionModel();

        String text = filterField.getText();

        if (text.isEmpty()) {
            return;
        }

        list = database.filter(text);

        if (list == null || list.isEmpty()) {
            labelFilter.setStyle("-fx-text-fill: red;");
            labelFilter.setAlignment(Pos.CENTER);
            labelFilter.setText("Not found!");
            filterField.setVisible(false);
            buttonFilter.setVisible(false);
            return;
        }

        selectionModel.select(serviceTab);

        servicePagination.setPageFactory(pageIndex -> {
            int fromIndex = pageIndex * itemsPerPage;
            int toIndex = fromIndex + itemsPerPage;
            int maxIndex = (int) floor((double) list.size() / itemsPerPage);

            if (servicePagination.getCurrentPageIndex() < maxIndex) {
                serviceTable.setItems(FXCollections.observableArrayList(
                        list.subList(fromIndex, toIndex))
                );
            } else {
                serviceTable.setItems(FXCollections.observableArrayList(
                        list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage)))
                );
            }
            return serviceTable;
        });
        servicePagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
        back();
    }

    @FXML
    void importDatabaseWithOpen(String filepath) {
        File file;
        String path;
        if (filepath.isEmpty()) {
            path = importField.getText();
            file = new File(path);
        } else {
            path = filepath;
            file = new File(path);
        }


        if (path.isEmpty()) {
            try {
                String url = "jdbc:sqlite:resources/database/database.db";
                database = new DatabaseImporter(url);

                try {
                    database.isEmpty();
                } catch (Exception e) {
                    database.createTables();
                    String xlsxfilepath = "resources/database/data-4272-2023-09-13.xlsx";
                    String jsonfilepath = "resources/database/metro.json";
                    String csvfilepath = "resources/database/Houses.csv";

                    JsonReader readerJSON = new JsonReader(jsonfilepath);
                    CSVReader readerCSV = new CSVReader(csvfilepath);
                    XlsxReader readerXLSX = new XlsxReader(xlsxfilepath);

                    readerJSON.readMetroStations(database);
                    readerCSV.importDb(database);
                    readerXLSX.readDomesticServices(database);
                }
                reloadDatabase();

                servicePagination.setVisible(true);
                metStatPagination.setVisible(true);
                housesPagination.setVisible(true);
                popPagination.setVisible(true);
                metroStatisticPagination.setVisible(true);

                databaseName.setText(path);
                importPane.setVisible(true);
                labelImport.setStyle("-fx-text-fill: black;");
                labelImport.setAlignment(Pos.CENTER);
                importField.setVisible(false);
                labelImport.setText("Data were updated by default paths!");

                buttonImport.setVisible(false);
            } catch (Exception e) {
                importPane.setVisible(true);
                labelImport.setStyle("-fx-text-fill: red;");
                labelImport.setAlignment(Pos.CENTER);
                labelImport.setText("Error! Necessary files by default did not exist!");
                importField.setVisible(false);
                buttonImport.setVisible(false);
            }
            return;
        }

        if (!file.exists() || file.isDirectory()) {
            importPane.setVisible(true);
            labelImport.setStyle("-fx-text-fill: red;");
            labelImport.setAlignment(Pos.CENTER);
            labelImport.setText("Error! Your path is wrong!");
            importField.setVisible(false);
            buttonImport.setVisible(false);
            return;
        }

        try {
            String URL = "jdbc:sqlite:" + path;
            database = new DatabaseImporter(URL);
            reloadDatabase();
            servicePagination.setVisible(true);
            metStatPagination.setVisible(true);
            housesPagination.setVisible(true);
            popPagination.setVisible(true);
            metroStatisticPagination.setVisible(true);

            databaseName.setText(path);
            importPane.setVisible(true);
            labelImport.setStyle("-fx-text-fill: black;");
            labelImport.setAlignment(Pos.CENTER);
            importField.setVisible(false);
            labelImport.setText("Data were updated!");
            buttonImport.setVisible(false);
        } catch (Exception e) {
            System.err.println("Error! Database were broke!");
        }
    }

    @FXML
    void openFile() {
        File file;
        FileChooser fileChooser;

        try {
            fileChooser = new FileChooser();
            file = fileChooser.showOpenDialog(new Stage());

            if (file == null || !file.exists() || file.isDirectory()) {
                importPane.setVisible(true);
                labelImport.setStyle("-fx-text-fill: red;");
                labelImport.setAlignment(Pos.CENTER);

                if (file == null) {
                    labelImport.setText("Error! You did not choice file!");
                } else {
                    labelImport.setText("Error! Your path is wrong!");
                }

                importField.setVisible(false);
                buttonImport.setVisible(false);
                return;
            }
        } catch (Exception e) {
            importPane.setVisible(true);
            labelImport.setStyle("-fx-text-fill: red;");
            labelImport.setAlignment(Pos.CENTER);
            labelImport.setText("Error! The default directory is empty! Import the database yourself!");
            buttonImport.setVisible(false);
            return;
        }
        importDatabaseWithOpen(file.getPath());
        databaseName.setText(file.getName());
        importPane.setVisible(true);
        labelImport.setStyle("-fx-text-fill: black;");
        labelImport.setAlignment(Pos.CENTER);
        importField.setVisible(false);
        labelImport.setText("Data were updated!");
        buttonImport.setVisible(false);
    }

    @FXML
    void reloadDatabase() {
        if (database == null || database.isEmpty()) {
            return;
        }

        String selectedTab = tabPane.getSelectionModel().getSelectedItem().getId();

        switch (selectedTab) {
            case "serviceTab": {
                ArrayList<DomesticServices> list = database.getService();

                servicePagination.setPageFactory(pageIndex -> {
                    int fromIndex = pageIndex * itemsPerPage;
                    int toIndex = fromIndex + itemsPerPage;
                    int maxIndex = (int) floor((double) list.size() / itemsPerPage);

                    if (servicePagination.getCurrentPageIndex() < maxIndex) {
                        serviceTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, toIndex))
                        );
                    } else {
                        serviceTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage)))
                        );
                    }
                    return serviceTable;
                });
                servicePagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
                break;
            }
            case "metStatTab": {
                ArrayList<MetroStation> list = database.getMetroStations();

                metStatPagination.setPageFactory(pageIndex -> {
                    int fromIndex = pageIndex * itemsPerPage;
                    int toIndex = fromIndex + itemsPerPage;
                    int maxIndex = (int) floor((double) list.size() / itemsPerPage);

                    if (metStatPagination.getCurrentPageIndex() < maxIndex) {
                        metStatTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, toIndex))
                        );
                    } else {
                        metStatTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage)))
                        );
                    }
                    return metStatTable;
                });
                metStatPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
                break;
            }
            case "housesTab": {
                ArrayList<Houses> list = database.getHouses();

                housesPagination.setPageFactory(pageIndex -> {
                    int fromIndex = pageIndex * itemsPerPage;
                    int toIndex = fromIndex + itemsPerPage;
                    int maxIndex = (int) floor((double) list.size() / itemsPerPage);

                    if (housesPagination.getCurrentPageIndex() < maxIndex) {
                        housesTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, toIndex))
                        );
                    } else {
                        housesTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage)))
                        );
                    }
                    return housesTable;
                });
                housesPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
                break;
            }
            case "popTab": {
                ArrayList<DomesticServices> list = database.getService();

                popPagination.setPageFactory(pageIndex -> {
                    int fromIndex = pageIndex * itemsPerPage;
                    int toIndex = fromIndex + itemsPerPage;
                    int maxIndex = (int) floor((double) list.size() / itemsPerPage);

                    if (popPagination.getCurrentPageIndex() < maxIndex) {
                        popTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, toIndex))
                        );
                    } else {
                        popTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage)))
                        );
                    }
                    return popTable;
                });
                popPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
                break;
            }
            case "metroStatisticTab": {
                ArrayList<MetroStation> list = database.getMetroStations();

                metroStatisticPagination.setPageFactory(pageIndex -> {
                    int fromIndex = pageIndex * itemsPerPage;
                    int toIndex = fromIndex + itemsPerPage;
                    int maxIndex = (int) floor((double) list.size() / itemsPerPage);

                    if (metroStatisticPagination.getCurrentPageIndex() < maxIndex) {
                        metroStatisticTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, toIndex))
                        );
                    } else {
                        metroStatisticTable.setItems(FXCollections.observableArrayList(
                                list.subList(fromIndex, fromIndex + (list.size() % itemsPerPage)))
                        );
                    }
                    return metroStatisticTable;
                });
                metroStatisticPagination.setPageCount((int) ceil((double) list.size() / itemsPerPage));
                break;
            }
        }
    }
}
