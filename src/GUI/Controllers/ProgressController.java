package GUI.Controllers;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ProgressIndicator;
import javafx.stage.Stage;
import src.DatabaseImporter;

import java.sql.SQLException;


public class ProgressController {

    @FXML
    private ProgressIndicator loadingIndicator;

    @FXML
    private Label completionLabel;

    @FXML
    private Button exitButton;

    // Ваши другие методы и код контроллера

    @FXML
    void exit() {
        // Получение ссылки на текущее окно
        Stage stage = (Stage) exitButton.getScene().getWindow();

        // Закрытие окна
        stage.close();
    }


    private static MenuController controller = null;

//    public static void setDatabase(DatabaseImporter database) {
//        ProgressController.database = database;
//    }


    public void startTask(MenuController menuController) {
        controller = menuController;
        String url = "jdbc:sqlite:resources/database/database.db";
        DatabaseImporter database = new DatabaseImporter(url);
        Task<Void> task = new Task<>() {
            @Override
            protected Void call() throws Exception {
                // Ваш код для выполнения задачи с обновлением прогресса
                Platform.runLater(() -> loadingIndicator.setVisible(true));
                database.metroStationsStatistic();
//                database.PopulationStatistic();
                Platform.runLater(() -> loadingIndicator.setVisible(false));
                return null;
            }
        };

        // Установка действия по завершении задачи
        task.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent event) {
                // Обновляем таблицы в интерфейсе после завершения задачи
                controller.updatePopulationTable();
                controller.updateMetroStationTable();
                taskComplete();
            }
        });

        // Запуск задачи в фоновом потоке
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    private void taskComplete() {
        // Покажите сообщение о завершении загрузки
        Platform.runLater(() -> completionLabel.setText("Статистика посчитана"));
    }
}